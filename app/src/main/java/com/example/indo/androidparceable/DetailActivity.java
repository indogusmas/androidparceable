package com.example.indo.androidparceable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    private  House house;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        house = getIntent().getParcelableExtra("HouseKey");
        if (house != null){
            initialData();
        }
    }

    private void initialData() {
        TextView tvPrice = findViewById(R.id.txtprice);
        tvPrice.setText("Price "+ Integer.toString(house.getPrice()));

        TextView tvLocation = findViewById(R.id.txtlocation);
        tvLocation.setText("Location "+ house.getLocation());

        TextView tvNearSchool = findViewById(R.id.txtnearschool);
        tvNearSchool.setText("Near School ? "+ ((Boolean)house.isNearSchool()).toString());

        TextView tvPrevious = findViewById(R.id.txtpreviousowner);
        tvPrevious.setText("Previous Owner "+ (house.getPreviousOwners()).toString());
    }
}
