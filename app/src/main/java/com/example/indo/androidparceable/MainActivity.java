package com.example.indo.androidparceable;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity  {

    House house;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        Create previos owner array
         */
        ArrayList<String>previousOwnerArray = new ArrayList<>();
        previousOwnerArray.add("Bang");
        previousOwnerArray.add("J");

        //Create an intance of haouse
        house = new House(10000,"Jakarta", true, previousOwnerArray);
        attachOnClickListener();
    }

    private void attachOnClickListener() {
        Button button = findViewById(R.id.btnparce);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lauchDetail(house);
            }
        });
    }

    private void lauchDetail(House house) {
        //Create Intent
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("HouseKey", house);
        startActivity(intent);
    }


}
